#include <stdio.h>
#include <stdlib.h>

#include <string.h>

#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <syslog.h>
#include <unistd.h>

#define LOCALPORT 9999
#define BUFFER_LEN 2048
#define DIR_LEN 4096

#define ERROR_SOCKET_CREATE -1
#define ERROR_SOCKET_CONNECT -2
#define ERROR_SOCKET_BIND -3
#define ERROR_SOCKET_LISTEN -4

#define ERROR_READ -700
#define ERROR_WRITE -70

#define ROOT_UID 0
#define USER_UID 1

#define CONFIRM_READ "READ"
#define EXIT_COMMAND "EXIT"

int network_socket;
struct sockaddr_in server_address;
int connection_status;
char output_msg[BUFFER_LEN];
char input_msg[BUFFER_LEN];
char terminal_in[BUFFER_LEN];
char temp[BUFFER_LEN];
int rw_flag;
int uid;
char curr_db_path[DIR_LEN];
int command_word_count;
char** parsed_command;

void error(int errcode){
    switch(errcode){
        case ERROR_SOCKET_CREATE:
            printf("FAILED TO CREATE SOCKET\n");
            break;
        case ERROR_SOCKET_CONNECT:
            printf("FAILED TO CONNECT SOCKET\n");
        case ERROR_READ:
            printf("FAILED TO READ FROM SOCKET\n");
        case ERROR_WRITE:
            printf("FAILED TO WRITE TO SOCKET\n");
        default:
            break;
    }

    exit(errcode);
}

void clear(char buf[], int len){
    for(int i = 0; i < len; i++) buf[i] = '\0';
}

void read_msg(){
    clear(input_msg, sizeof(input_msg));
    rw_flag = read(network_socket, input_msg, BUFFER_LEN);
    if(rw_flag < 0) error(ERROR_READ);
    // printf("INPUT FROM SERVER: %s\n", input_msg);

    // confirm_read();
}

void get_curr_path(){
    clear(curr_db_path, sizeof(curr_db_path));
    rw_flag = read(network_socket, curr_db_path, sizeof(curr_db_path));
    if(rw_flag < 0) error(ERROR_READ);
    // printf("GOT PATH FROM SERVER: %s\n", curr_db_path);

    // confirm_read();
}

void write_msg(const char* msg){
    clear(output_msg, sizeof(output_msg));
    strcpy(output_msg, msg);
    rw_flag = write(network_socket, output_msg, strlen(output_msg));
    if(rw_flag < 0) error(ERROR_WRITE);
    // printf("OUTPUT TO SERVER: %s\n", output_msg);

    // check_read();
    sleep(1);
}

int custom_tok(char str[], const char * delim, int start){
    if(start >= strlen(str)) return strlen(str);
    int flag;
    int i;
    int j;
    for(i = start; i < strlen(str); i++){
        // printf("str[i] %c == delim[0] %c ? %d\n", str[i], delim[0], str[i] == delim[0]);
        if(str[i] == delim[0]){
            flag = 0;
            for(j = 0; j < strlen(delim) && j < strlen(str) - i; j++){
                if(!(str[i + j] == delim[j])) flag = 1;
            }
            if(!flag) return i;
        }
    }

    return strlen(str);
}

int count_token(char str[], const char* delim){
    int count = 0;
    int start = 0;
    int end_of_token = custom_tok(str, delim, start);
    while(start < strlen(str)){
        count++;
        // for(int i = start; i < end_of_token; i++) printf("%c", str[i]);
        // printf("\n");
        start = end_of_token + strlen(delim);
        if(end_of_token < strlen(str))end_of_token = custom_tok(str, delim, start);
        else break;
        // printf("%d %d %lu\n", start, end_of_token, strlen(str));
    }

    return count;
}

char** create_token_array(char str[], const char* delim, int* size){
    int count = count_token(str, delim);
    *size = count;
    char** arr = (char**) malloc(count * (sizeof(char*)));
    for(int i = 0; i < count; i++){
        arr[i] = (char*) malloc(256 * sizeof(char));
    }

    count = 0;
    int start = 0;
    int end_of_token = custom_tok(str, delim, start);
    char char_to_append[2];
    while(start < strlen(str)){
        clear(arr[count], sizeof(arr[count]));
        strcpy(arr[count], "");
        for(int i = start; i < end_of_token; i++){
            sprintf(char_to_append, "%c", str[i]);
            strcat(arr[count], char_to_append);
        }
        start = end_of_token + strlen(delim);
        if(end_of_token < strlen(str))end_of_token = custom_tok(str, delim, start);
        else break;
        // printf("%d %d %lu\n", start, end_of_token, strlen(str));
        count++;
    }

    return arr;
}

void client_select(){
    //read header
    read_msg();
    printf("\n%s\n", input_msg);

    //read row count
    read_msg();
    int row_count = atoi(input_msg);

    for(int i = 0; i < row_count; i++){
        read_msg();
        printf("%s\n", input_msg);
    }

    printf("\n");
}

int main(int argc, char* argv[]){
    char user[BUFFER_LEN/4];
    char pass[BUFFER_LEN/4];

    //CREATE SOCKET
    network_socket = socket(AF_INET, SOCK_STREAM, 0);
    if(network_socket < 0) error(ERROR_SOCKET_CREATE);

    //SET SOCKET ADDRESS UP
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(LOCALPORT);
    server_address.sin_addr.s_addr = INADDR_ANY;

    connection_status = connect(network_socket, (struct sockaddr *) &server_address, sizeof(server_address));
    if(connection_status < 0) error(ERROR_SOCKET_CONNECT);

    //SEND USER TYPE
    uid = getuid();
    if(uid == ROOT_UID) {
        // printf("LOGGED IN AS ROOT\n\n");
        write_msg("ROOT");
        strcpy(user, "ROOT");
    }else {
        for(int i = 0; i < argc; i++){
            // printf("ARGUMENT %d: %s\n", i, argv[i]);
            if(!strcmp(argv[i], "-u")) {
                if(i < argc - 1){
                    clear(user, sizeof(user));
                    strcpy(user, argv[++i]);
                }
            }if(!strcmp(argv[i], "-p")) {
                if(i < argc - 1){
                    // printf("BEFORE CLEAR USER: %s\nPASS: %s\n", user, pass);
                    clear(pass, sizeof(pass));
                    // printf("AFTER CLEAR USER: %s\nPASS: %s\n", user, pass);
                    strcpy(pass, argv[++i]);
                }
            }
        }


        // printf("USER: %s\nPASS: %s\n", user, pass);
        // printf("LOGGED IN AS USER\n\n");
        clear(temp, sizeof(temp));
        sprintf(temp, "%s\t%s\n", user, pass);
        write_msg(temp);
    }
    
    read_msg();
    // printf("LOGIN STATUS: ---%s---\n", input_msg);
    if(!strcmp(input_msg, "LOGIN FAILED")){
        return 0;
    }

    while(1){
        // printf("\n");
        get_curr_path();
        //GET QUERY FROM TERMINAL
        printf("\n%s@%s$>> ", user, curr_db_path);
        clear(terminal_in, sizeof(terminal_in));
        fgets(terminal_in, BUFFER_LEN, stdin);
        terminal_in[strlen(terminal_in)-1] = '\0';
        if(terminal_in[strlen(terminal_in)-1] == ';')terminal_in[strlen(terminal_in)-1] = '\0';
        parsed_command = create_token_array(terminal_in, " ", &command_word_count);
        // printf("SEND TO SERVER: %s-----\n", terminal_in);

        //SEND COMMAND
        //QWRITE 1

        write_msg(terminal_in);
        
        //RECEIVE CONFIRMATION
        //QREAD 1
        read_msg();
        
        //END PROGRAM IF REQUESTED
        if(!strcmp(output_msg, EXIT_COMMAND)) break;
        if(!strcmp(parsed_command[0], "SELECT")) client_select();
        //RECEIVE QUERY RESULT
        //QREAD2
        read_msg();
        printf("STATUS: %s\n\n", input_msg);
    }

    close(network_socket);
    return 0;
}