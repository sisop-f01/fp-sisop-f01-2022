#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <time.h>

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <syslog.h>
#include <unistd.h>

#define LOCALPORT 9999
#define BUFFER_LEN 2048
#define DIR_LEN BUFFER_LEN * 2

#define ERROR_SOCKET_CREATE -1
#define ERROR_SOCKET_CONNECT -2
#define ERROR_SOCKET_BIND -3
#define ERROR_SOCKET_LISTEN -4

#define ERROR_SYNTAX -5
#define ERROR_UNAUTHORIZED_OP -6
#define ERROR_UNRECOGNIZED_CMD -7

#define ERROR_DIR_EXISTS -8
#define ERROR_DIR_NOT_EXIST -9
#define ERROR_MKDIR -10

#define ERROR_NOT_CONNECTED -11
#define ERROR_DB_PERMISSION -12

#define ERROR_USER_NOT_EXIST -13
#define ERROR_DATATYPE_NOT_EXIST -14

#define ERROR_FILE_EXISTS -15
#define ERROR_FILE_NOT_EXIST -16

#define ERROR_RMDIR -17
#define ERROR_ALREADY_AUTHORIZED -18

#define ERROR_COLUMN_EXISTS -19
#define ERROR_COLUMN_NOT_EXIST -20

#define ERROR_INCOMPATIBLE_DATATYPE -21

#define ERROR_OPEN -7000
#define ERROR_READ -700
#define ERROR_WRITE -70

#define SUCCESS_USER_ADD 1
#define SUCCESS_DB_ADD 2
#define SUCCESS_CONNECT 3
#define SUCCESS_DISCONNECT 4
#define SUCCESS_TABLE_ADD 5
#define SUCCESS_DROP_DB 6
#define SUCCESS_DROP_TABLE 7
#define SUCCESS_GRANT 8
#define SUCCESS_DROP_COLUMN 9
#define SUCCESS_INSERT 10
#define SUCCESS_UPDATE 11
#define SUCCESS_DELETE 12
#define SUCCESS_SELECT 13

#define ROOT_UID 0
#define USER_UID 1

#define PERMISSION_OWNER 7
#define PERMISSION_RW 6
#define PERMISSION_R 4
#define PERMISSION_W 2

#define CONFIRM_READ "READ"
#define EXIT_COMMAND "EXIT"

#define USER_LIST_PATH "/home/nfpurnama/FP_TEST/PseudoSQL/userTEST2.txt"
#define CUSTOM_DB_PATH "/home/nfpurnama/FP_TEST/PseudoSQL/"
#define PERMISSION_FILENAME "/permission.txt"
#define LOG_PATH "/home/nfpurnama/FP_TEST/PseudoSQL/history.log"

int server_socket;
struct sockaddr_in server_address;
int bind_status;
int listen_status;
int client_socket;
int addr_size = sizeof(server_address);
char output_msg[BUFFER_LEN];
char input_msg[BUFFER_LEN];
char temp[BUFFER_LEN];
char user[BUFFER_LEN/4];
char pass[BUFFER_LEN/4];
int uid;
int rw_flag;
char curr_db_path[DIR_LEN];
int command_word_count;
char** parsed_command;

void check(int call, int errcode){
    if(call < 0){
        exit(errcode);
    }
}

void clear(char buf[], int len){
    for(int i = 0; i < len; i++) buf[i] = '\0';
}

void error(int errcode){
    exit(errcode);
}

void read_msg(){
    clear(input_msg, sizeof(input_msg));
    rw_flag = read(client_socket, input_msg, BUFFER_LEN);
    if(rw_flag < 0) error(ERROR_READ);
    printf("INPUT FROM CLIENT: %s\n", input_msg);

    // confirm_read();
}

void write_msg(const char* msg){
    clear(output_msg, sizeof(output_msg));
    strcpy(output_msg, msg);
    rw_flag = write(client_socket, output_msg, strlen(output_msg));
    if(rw_flag < 0) error(ERROR_WRITE);
    printf("OUTPUT TO CLIENT: %s\n", output_msg);

    // check_read();
    sleep(1);
}

void send_curr_path(){
    rw_flag = write(client_socket, curr_db_path, strlen(curr_db_path));
    if(rw_flag < 0) error(ERROR_WRITE);
    printf("SEND PATH TO CLIENT: %s\n", curr_db_path);

    // check_read();
    sleep(1);
}

void msg(int code){
    clear(temp, sizeof(temp));
    switch(code){
       case ERROR_SYNTAX:
            strcpy(temp, "SYNTAX ERROR");
            break;
       case ERROR_UNAUTHORIZED_OP:
            strcpy(temp, "USER UNAUTHORIZED TO CONDUCT OPERATION");
            break;
       case ERROR_UNRECOGNIZED_CMD:
            strcpy(temp, "UNRECOGNIZED COMMAND");
            break;
       case ERROR_DIR_EXISTS:
            strcpy(temp, "DATABASE ALREADY EXISTS");
            break;
       case ERROR_DIR_NOT_EXIST:
            strcpy(temp, "DATABASE NOT FOUND");
            break;
       case ERROR_FILE_EXISTS:
            strcpy(temp, "FILE ALREADY EXISTS");
            break;
       case ERROR_FILE_NOT_EXIST:
            strcpy(temp, "FILE NOT FOUND");
            break;
       case ERROR_COLUMN_EXISTS:
            strcpy(temp, "COLUMN ALREADY EXISTS");
            break;
       case ERROR_COLUMN_NOT_EXIST:
            strcpy(temp, "COLUMN NOT FOUND");
            break;
       case ERROR_MKDIR:
            strcpy(temp, "MKDIR FUNCTION FAIL");
            break;
       case ERROR_NOT_CONNECTED:
            strcpy(temp, "NOT CONNECTED TO A DATABASE");
            break;
       case ERROR_DB_PERMISSION:
            strcpy(temp, "USER NOT PERMITTED TO ACCESS DATABASE");
            break;
       case ERROR_USER_NOT_EXIST:
            strcpy(temp, "USER DOES NOT EXIST");
            break;
       case ERROR_DATATYPE_NOT_EXIST:
            strcpy(temp, "DATATYPE DOES NOT EXIST");
            break;
       case ERROR_ALREADY_AUTHORIZED:
            strcpy(temp, "USER ALREADY AUTHORIZED");
            break;
       case ERROR_INCOMPATIBLE_DATATYPE:
            strcpy(temp, "INPUT VALUE INCOMPATIBLE WITH COLUMN");
            break;
       case ERROR_OPEN:
            strcpy(temp, "FAILED TO OPEN FILE/DIRECTORY");
            break;
       case ERROR_READ:
            strcpy(temp, "FAILED TO READ FILE");
            break;
       case ERROR_WRITE:
            strcpy(temp, "FAILED TO WRITE FILE");
            break;
       case SUCCESS_USER_ADD:
            strcpy(temp, "USER ADDED");
            break;
       case SUCCESS_DB_ADD:
            strcpy(temp, "DATABASE ADDED");
            break;
       case SUCCESS_CONNECT:
            strcpy(temp, "CONNECTED TO DATABASE");
            break;
       case SUCCESS_DISCONNECT:
            strcpy(temp, "DISCONNECTED FROM DATABASE");
            break;
       case SUCCESS_TABLE_ADD:
            strcpy(temp, "TABLE ADDED");
            break;
       case SUCCESS_DROP_DB:
            strcpy(temp, "DATABASE DROPPED");
            break;
       case SUCCESS_DROP_TABLE:
            strcpy(temp, "TABLE DROPPED");
            break;
       case SUCCESS_DROP_COLUMN:
            strcpy(temp, "COLUMN DROPPED");
            break;
       case SUCCESS_GRANT:
            strcpy(temp, "ACCESS GRANTED");
            break;
       case SUCCESS_INSERT:
            strcpy(temp, "VALUES INSERTED INTO TABLE");
            break;
       case SUCCESS_UPDATE:
            strcpy(temp, "TABLE UPDATED");
            break;
       case SUCCESS_DELETE:
            strcpy(temp, "QUALIFIED ROWS DELETED");
            break;
       case SUCCESS_SELECT:
            strcpy(temp, "QUALIFIED ROWS SELECTED");
            break;
        default:
            strcpy(temp, "NO MESSAGE");
            break;
    }
    write_msg(temp);
}

void omit_column(char temp[BUFFER_LEN], char edited_line[BUFFER_LEN], int size, int count){
    clear(edited_line, size);
    strcpy(edited_line, "");

    char *token;
    token = strtok(temp, "\t");
    int tab_count = 1;

    while(token){
        // printf("TOKEN ---%s%d---", token, strcmp(token, "\n"));
        if(tab_count != count && strcmp(token, "\n") != 0){
            strcat(edited_line, token);
            strcat(edited_line, "\t");
            printf("YES");
        }
        token = strtok(NULL, "\t");
        tab_count++;
        printf("\n");
    }
}

int check_int(char* token){
    int flag = 1;
    for(int i = 0; i < strlen(token); i++){
        if(!(token[i] >= '0' && token[i] <= '9')){
            flag = 0;
            break;
        }
    }

    return flag;
}

int check_float(char* token){
    int flag = 1;
    int comma_count = 0;
    for(int i = 0; i < strlen(token); i++){
        if(!((token[i] >= '0' && token[i] <= '9') || (token[i] == ',' && !comma_count))){
            flag = 0;
            break;
        }
        if(token[i] == ',') comma_count++;
    }

    return flag && comma_count;
}

int check_string(char* token){
    return (token[0] == '"' && token[strlen(token) - 1] == token[0]);
}

int login_check(char test[BUFFER_LEN]){
    char input[BUFFER_LEN];
    strcpy(input, test);
    FILE *user_list = fopen(USER_LIST_PATH, "r");
    // printf("INPUT AFTER FILE %s\n", input);
    if(user_list){
        // printf("INPUT BEFORE CLEAR TEMP %s\n", input);
        clear(temp, sizeof(temp));
        // printf("INPUT AFTER CLEAR TEMP BEFORE WHILE %s\n", input);
        while(fgets(temp, BUFFER_LEN, user_list)){
            // printf("INPUT BEFORE PRINT %s\n", input);
            // printf("INPUT IS %s\nLINE IS %s\nSTRCMP IS %d\n\n", input, temp, strcmp(temp, input));
            if(!strcmp(temp, input)) {
                char* token = strtok(temp, "\t");
                clear(user, sizeof(user));
                strcpy(user, token);
                token = strtok(NULL, "\t");
                clear(pass, sizeof(pass));
                strcpy(pass, token);
                return 1;
            }clear(temp, sizeof(temp));
            // printf("INPUT END OF WHILE LOOP %s\n", input);
        }
    }

    return 0;
}

int custom_tok(char str[], const char * delim, int start){
    if(start >= strlen(str)) return strlen(str);
    int flag;
    int i;
    int j;
    for(i = start; i < strlen(str); i++){
        // printf("str[i] %c == delim[0] %c ? %d\n", str[i], delim[0], str[i] == delim[0]);
        if(str[i] == delim[0]){
            flag = 0;
            for(j = 0; j < strlen(delim) && j < strlen(str) - i; j++){
                if(!(str[i + j] == delim[j])) flag = 1;
            }
            if(!flag) return i;
        }
    }

    return strlen(str);
}

int count_token(char str[], const char* delim){
    int count = 0;
    int start = 0;
    int end_of_token = custom_tok(str, delim, start);
    while(start < strlen(str)){
        count++;
        // for(int i = start; i < end_of_token; i++) printf("%c", str[i]);
        // printf("\n");
        start = end_of_token + strlen(delim);
        if(end_of_token < strlen(str))end_of_token = custom_tok(str, delim, start);
        else break;
        // printf("%d %d %lu\n", start, end_of_token, strlen(str));
    }

    return count;
}

char** create_token_array(char str[], const char* delim, int* size){
    int count = count_token(str, delim);
    *size = count;
    char** arr = (char**) malloc(count * (sizeof(char*)));
    for(int i = 0; i < count; i++){
        arr[i] = (char*) malloc(256 * sizeof(char));
    }

    count = 0;
    int start = 0;
    int end_of_token = custom_tok(str, delim, start);
    char char_to_append[2];
    while(start < strlen(str)){
        clear(arr[count], sizeof(arr[count]));
        strcpy(arr[count], "");
        for(int i = start; i < end_of_token; i++){
            sprintf(char_to_append, "%c", str[i]);
            strcat(arr[count], char_to_append);
        }
        start = end_of_token + strlen(delim);
        if(end_of_token < strlen(str))end_of_token = custom_tok(str, delim, start);
        else break;
        // printf("%d %d %lu\n", start, end_of_token, strlen(str));
        count++;
    }

    return arr;
}

int sql_create(char msg[BUFFER_LEN], int uid){
    printf("\n\nENTERED TOKENIZER\n\n");
    char command[BUFFER_LEN];
    clear(command, sizeof(command));
    strcpy(command, msg);
    char* token = strtok(command, " ");
    
    if(token && !strcmp(token, "CREATE")){
        token = strtok(NULL, " ");
        if(token && !strcmp(token, "USER")){
            if(uid == ROOT_UID){
                token = strtok(NULL, " ");
                if(token){
                    char temp_user[BUFFER_LEN];
                    strcpy(temp_user, token);
                    token = strtok(NULL, " ");
                    if(!strcmp(token, "IDENTIFIED")){
                        token = strtok(NULL, " ");
                        if(!strcmp(token, "BY")){
                            token = strtok(NULL, " ");
                            if(token){
                                char temp_pass[BUFFER_LEN];
                                strcpy(temp_pass, token);
                                FILE * user_fp = fopen(USER_LIST_PATH, "a");
                                fprintf(user_fp,"%s\t%s\n", temp_user, temp_pass);
                                fclose(user_fp);
                                return SUCCESS_USER_ADD;
                            }else return ERROR_SYNTAX;
                        }else return ERROR_SYNTAX;
                    }else return ERROR_SYNTAX;
                }else return ERROR_SYNTAX;
            }else return ERROR_UNAUTHORIZED_OP;
        }
        else if(token && !strcmp(token, "DATABASE")){
                token = strtok(NULL, " ");
                if(token){
                    char new_db_path[DIR_LEN];
                    strcpy(new_db_path, CUSTOM_DB_PATH);
                    strcat(new_db_path, token);
                    DIR* dir = opendir(new_db_path);
                    if(dir){
                        closedir(dir);
                        return ERROR_DIR_EXISTS;
                    }else if(ENOENT == errno){
                        if(mkdir(new_db_path, 0777) < 0) return ERROR_MKDIR;
                        strcat(new_db_path, PERMISSION_FILENAME);
                        FILE* permission_file = fopen(new_db_path, "a");
                        fprintf(permission_file, "%s\t%d\n", user, PERMISSION_OWNER);
                        fclose(permission_file);
                        return SUCCESS_DB_ADD;
                    }else return ERROR_OPEN;
                }else return ERROR_SYNTAX;
        }
        else if(token && !strcmp(token, "TABLE")){
            if(!strcmp(curr_db_path, CUSTOM_DB_PATH)) return ERROR_NOT_CONNECTED;
            token = strtok(NULL, " ");
            if(token){
                char table_name[strlen(token)];
                clear(table_name, sizeof(table_name));
                strcpy(table_name, token);

                char new_table_path[DIR_LEN * 2];
                clear(new_table_path, sizeof(BUFFER_LEN));
                sprintf(new_table_path, "%s/%s.txt", curr_db_path, table_name);
                printf("NEW TABLE NAME: %s\n", new_table_path);

                FILE* new_table = fopen(new_table_path, "r");
                if(new_table){
                    printf("TABLE EXISTS\n");
                    fclose(new_table);
                    return ERROR_FILE_EXISTS;
                }

                char column_names[BUFFER_LEN];
                char column_data_types[BUFFER_LEN];
                clear(column_names, sizeof(column_names));
                clear(column_data_types, sizeof(column_data_types));
                token = strtok(NULL, " (");
                printf("\n\nTOKEN OF COL_VAL: %s----\n\n", token);
                while(token){
                    char column[BUFFER_LEN];
                    strcpy(column, token);
                    int i = strlen(column);
                    while(!(column[i] >= 'a' && column[i] <= 'z') 
                    && 
                    !(column[i] >= 'A' && column[i] <= 'Z')){
                        column[i--] = '\0';
                    }
                    strcat(column_names, column);
                    strcat(column_names, "\t");


                    token = strtok(NULL, " ");
                    if(token){
                        char data_type[BUFFER_LEN];
                        strcpy(data_type, token);
                        i = strlen(data_type);
                        while(!(data_type[i] >= 'a' && data_type[i] <= 'z') 
                        && 
                        !(data_type[i] >= 'A' && data_type[i] <= 'Z')){
                            data_type[i--] = '\0';
                        }
                        if(!strcmp(data_type, "int") || !strcmp(data_type, "float") || !strcmp(data_type, "string")){
                            strcat(column_data_types, data_type);
                            strcat(column_data_types, "\t");
                        }else return ERROR_DATATYPE_NOT_EXIST;
                    }else return ERROR_SYNTAX;
                    token = strtok(NULL, " ");
                }
                printf("table: --%s--\ncolumns: --%s--\ndata tp: --%s--\n\n", table_name, column_names, column_data_types);
                //WRITE TABLE INSIDE CURR_DB_PATH

                printf("TABLE NOT EXIST\n");
                new_table = fopen(new_table_path, "a");
                fprintf(new_table, "%s\n%s\n", column_names, column_data_types);
                fclose(new_table);
                //WRITE TABLE INSIDE CURR_DB_PATH
                return SUCCESS_TABLE_ADD;
            }else return ERROR_SYNTAX;
        }else return ERROR_SYNTAX;
    }else return ERROR_SYNTAX;
}

int sql_use(char msg[BUFFER_LEN], int uid){
    printf("\n\nENTERED TOKENIZER\n\n");
    char command[BUFFER_LEN];
    clear(command, sizeof(command));
    strcpy(command, msg);
    char* token = strtok(command, " ");
    if(!strcmp(token, "USE")){
        token = strtok(NULL, " ");
        if(!strcmp(token, "DATABASE")){
            token = strtok(NULL, " ");
            if(token){
                char new_db_path[DIR_LEN];
                strcpy(new_db_path, CUSTOM_DB_PATH);
                strcat(new_db_path, token);
                DIR* dir = opendir(new_db_path);
                if(dir){
                    char db_permission_path[DIR_LEN];
                    strcpy(db_permission_path, new_db_path);
                    strcat(db_permission_path, PERMISSION_FILENAME);
                    if(uid == ROOT_UID){
                        clear(curr_db_path, sizeof(curr_db_path));
                        strcpy(curr_db_path, CUSTOM_DB_PATH);
                        strcat(curr_db_path, token);
                        return SUCCESS_CONNECT;
                    }else{
                        int code = ERROR_DB_PERMISSION;
                        FILE *permission_file = fopen(db_permission_path, "r");
                        while(fgets(temp, sizeof(temp), permission_file)){
                            char *user_token = strtok(temp, "\t");
                            printf("-%s- token:-%s-\n", user, user_token);
                            if(!strcmp(user_token, user)) {
                                clear(curr_db_path, sizeof(curr_db_path));
                                strcpy(curr_db_path, CUSTOM_DB_PATH);
                                strcat(curr_db_path, token);
                                code = SUCCESS_CONNECT;
                                break;
                            }
                            // printf("INPUT END OF WHILE LOOP %s\n", input);
                        }
                        fclose(permission_file);
                        return code;
                    }
                }else if(ENOENT == errno){
                    return ERROR_DIR_NOT_EXIST;
                }else return ERROR_OPEN;
            }else return ERROR_SYNTAX; 
        }else return ERROR_SYNTAX;
    }
    
    return 0;
}

int sql_disconnect(char msg[BUFFER_LEN], int uid){
    printf("\n\nENTERED TOKENIZER\n\n");
    char command[BUFFER_LEN];
    clear(command, sizeof(command));
    strcpy(command, msg);
    char* token = strtok(command, " ");
    if(!strcmp(token, "DISCONNECT")){
        token = strtok(NULL, " ");
        if(!strcmp(token, "DATABASE")){
            if(!strcmp(curr_db_path, CUSTOM_DB_PATH)) return ERROR_NOT_CONNECTED;
            strcpy(curr_db_path, CUSTOM_DB_PATH);
            return SUCCESS_DISCONNECT;
        }else return ERROR_SYNTAX; 
    }else return ERROR_SYNTAX;
    
    return 0;
}

int sql_grant(char msg[BUFFER_LEN], int uid){
    printf("\n\nENTERED TOKENIZER\n\n");
    char command[BUFFER_LEN];
    clear(command, sizeof(command));
    strcpy(command, msg);
    char* token = strtok(command, " ");
    if(!strcmp(token, "GRANT")){
        if(uid == ROOT_UID){
            token = strtok(NULL, " ");
            if(!strcmp(token, "PERMISSION")){
                token = strtok(NULL, " ");
                if(token){
                    char temp_db[BUFFER_LEN];
                    strcpy(temp_db, token);

                    char db_path[DIR_LEN];
                    strcpy(db_path, CUSTOM_DB_PATH);
                    strcat(db_path, temp_db);
                    
                    DIR* dir = opendir(db_path);
                    if(dir){
                    }else if(ENOENT == errno){
                        return ERROR_DIR_NOT_EXIST;
                    }else return ERROR_OPEN;
                    token = strtok(NULL, " ");
                    if(!strcmp(token, "INTO")){
                        token = strtok(NULL, " ");
                        if(token){
                            char temp_user[BUFFER_LEN];
                            strcpy(temp_user, token);

                            char db_permission_path[DIR_LEN];
                            strcpy(db_permission_path, db_path);
                            strcat(db_permission_path, PERMISSION_FILENAME);
                            
                            FILE *users_list = fopen(USER_LIST_PATH, "r");
                            clear(temp, sizeof(temp));

                            while(fgets(temp, BUFFER_LEN, users_list)){
                                char *temp_token = strtok(temp, "\t");
                                if(!strcmp(temp_token, temp_user)) {
                                    FILE *permission_file = fopen(db_permission_path, "r");
                                    while(fgets(temp, sizeof(temp), permission_file)){
                                        temp_token = strtok(temp, "\t");
                                        if(!strcmp(temp_token, temp_user)) return ERROR_ALREADY_AUTHORIZED;
                                    }
                                    fclose(permission_file);
                                    permission_file = fopen(db_permission_path, "a");
                                    fprintf(permission_file, "%s\t%d", temp_user, PERMISSION_RW);
                                    fclose(permission_file);
                                    return SUCCESS_GRANT;
                                }clear(temp, sizeof(temp));
                            }
                            return ERROR_USER_NOT_EXIST;
                        }else return ERROR_SYNTAX;
                    }else return ERROR_SYNTAX;
                }else return ERROR_SYNTAX;
            }else return ERROR_SYNTAX;
        }else return ERROR_UNAUTHORIZED_OP;
    }
    return 0;
}

int sql_drop(char msg[BUFFER_LEN], int uid){
    char command[BUFFER_LEN];
    clear(command, sizeof(command));
    strcpy(command, msg);
    char *token = strtok(command, " ");
    
    if(token && !strcmp(token, "DROP")){
        token = strtok(NULL, " ");
        if(token && !strcmp(token, "DATABASE")){
            token = strtok(NULL, " ");
            if(token){
                char db_path[DIR_LEN];
                strcpy(db_path, CUSTOM_DB_PATH);
                strcat(db_path, token);

                //CHECK USER PERMISSION OWNER OR ROOT
                if(uid != ROOT_UID){
                    char db_permission_path[DIR_LEN * 2];
                    sprintf(db_permission_path, "%s/%s", db_path, PERMISSION_FILENAME);

                    FILE *permission_file = fopen(db_permission_path, "r");
                    if(!permission_file) return ERROR_FILE_NOT_EXIST;
                    int flag = 0;
                    char test_line[BUFFER_LEN];
                    clear(test_line, sizeof(test_line));
                    sprintf(test_line, "%s\t%d\n", user, PERMISSION_OWNER);

                    clear(temp, sizeof(temp));
                    while(fgets(temp, sizeof(temp), permission_file)){
                        printf("TEMP: %s\n TEST: %s\n", temp, test_line);
                        if(!strcmp(temp, test_line)) {
                            flag = 1;
                            break;
                        }
                    }
                    fclose(permission_file);
                    if(!flag) return ERROR_UNAUTHORIZED_OP;
                }
                //

                DIR* dir = opendir(db_path);
                if(dir){
                    struct dirent* entity = readdir(dir);
                    char path_to_remove[DIR_LEN * 2];
                    while(entity){
                        if(entity->d_type == DT_REG){
                            clear(path_to_remove, sizeof(path_to_remove));
                            sprintf(path_to_remove, "%s/%s", db_path, entity->d_name);
                            printf("%s\n", path_to_remove);
                            remove(path_to_remove);
                        }
                        entity = readdir(dir);
                    }
                    if(rmdir(db_path) < 0) return ERROR_RMDIR;
                    strcpy(curr_db_path, CUSTOM_DB_PATH);
                    return SUCCESS_DROP_DB;
                }else if(ENOENT == errno){
                    return ERROR_DIR_NOT_EXIST;
                }else return ERROR_OPEN;
            }else return ERROR_SYNTAX;
        }
        
        else if(token && !strcmp(token, "TABLE")){
            if(!strcmp(curr_db_path, CUSTOM_DB_PATH)) return ERROR_NOT_CONNECTED;
            token = strtok(NULL, " ");

            //CHECK USER PERMISSION OWNER OR ROOT
            if(uid != ROOT_UID){
                char db_permission_path[DIR_LEN * 2];
                sprintf(db_permission_path, "%s/%s", curr_db_path, PERMISSION_FILENAME);

                FILE *permission_file = fopen(db_permission_path, "r");
                int flag = 0;
                char test_line[BUFFER_LEN];
                clear(test_line, sizeof(test_line));
                sprintf(test_line, "%s\t%d", user, PERMISSION_RW);

                clear(temp, sizeof(temp));
                while(fgets(temp, sizeof(temp), permission_file)){
                    printf("\n\nCHECKING-%s-%s-%d-\n", temp, test_line, strcmp(temp, test_line));
                    if(!strcmp(temp, test_line)) {
                        flag = 1;
                        break;
                    }
                    // printf("INPUT END OF WHILE LOOP %s\n", input);
                }

                if(!flag) return ERROR_UNAUTHORIZED_OP;
            }
            //

            if(token){
                char table_name[strlen(token)];
                clear(table_name, sizeof(table_name));
                strcpy(table_name, token);

                char table_path[DIR_LEN * 2];
                clear(table_path, sizeof(BUFFER_LEN));
                sprintf(table_path, "%s/%s.txt", curr_db_path, table_name);
                printf("TABLE TO BE REMOVED: %s\n", table_path);

                if(remove(table_path) < 0) return ERROR_FILE_NOT_EXIST;
                return SUCCESS_DROP_TABLE; 
            }else return ERROR_SYNTAX;
        }else if(token && !strcmp(token, "COLUMN")){
            if(!strcmp(curr_db_path, CUSTOM_DB_PATH)) return ERROR_NOT_CONNECTED;
            token = strtok(NULL, " ");
            if(token){
                char column_name[BUFFER_LEN];
                clear(column_name, sizeof(column_name));
                strcpy(column_name, token);
                token = strtok(NULL, " ");
                if(token && !strcmp(token, "FROM")){
                    token = strtok(NULL, " ");
                    if(token){
                        char table_name[BUFFER_LEN];
                        clear(table_name, sizeof(table_name));
                        strcpy(table_name, token);

                        //check if column exists
                        char table_path[DIR_LEN * 2];
                        sprintf(table_path, "%s/%s.txt", curr_db_path, table_name);
                        FILE *table = fopen(table_path, "r");
                        
                        if(table){
                            char columns[BUFFER_LEN];
                            clear(columns, sizeof(BUFFER_LEN));
                            fgets(columns, sizeof(columns), table);
                            
                            char data_types[BUFFER_LEN];
                            clear(data_types, sizeof(BUFFER_LEN));
                            fgets(data_types, sizeof(data_types), table);

                            fclose(table);
                            printf("%s\n%s\n", columns, data_types);
                            int count = 0;
                            clear(temp, sizeof(temp));
                            strcpy(temp, columns);
                            char* col_token = strtok(temp, "\t");
                            while(col_token){
                                count++;
                                printf("-%s-%s-\n", column_name, col_token);
                                if(!strcmp(column_name, col_token)) break;
                                col_token = strtok(NULL, "\t");
                            }
                            if(!col_token) return ERROR_COLUMN_NOT_EXIST;

                            table = fopen(table_path, "r");

                            clear(temp, sizeof(temp));
                            
                            char new_table_path[DIR_LEN * 2];
                            sprintf(new_table_path, "%s/tempTable.txt", curr_db_path);
                            FILE *new_table = fopen(new_table_path, "w");
                            if(!new_table) return ERROR_OPEN;

                            char edited_line[BUFFER_LEN];
                            while(fgets(temp, sizeof(temp), table)){
                                omit_column(temp, edited_line, sizeof(edited_line), count);
                                fprintf(new_table, "%s\n", edited_line);
                            }
                            fclose(new_table);
                            //REMOVE OLD FILE
                            remove(table_path);
                            //RENAME NEW FILE
                            rename(new_table_path, table_path);

                            return SUCCESS_DROP_COLUMN;
                        }else return ERROR_FILE_NOT_EXIST;
                    }else return ERROR_SYNTAX;
                }else return ERROR_SYNTAX;
            }else return ERROR_SYNTAX;
        }else return ERROR_SYNTAX;
    }else return ERROR_SYNTAX;
}

int sql_insert(char msg[BUFFER_LEN], int uid){
    char command[BUFFER_LEN];
    clear(command, sizeof(command));
    strcpy(command, msg);
    char *token = strtok(command, " ");
    
    if(!strcmp(curr_db_path, CUSTOM_DB_PATH)) return ERROR_NOT_CONNECTED;
    
    char permission_file_path[DIR_LEN * 2];
    sprintf(permission_file_path, "%s/%s", curr_db_path, PERMISSION_FILENAME);
    printf("PERMISSIONS FILE: %s\n", permission_file_path);
    FILE *permission_file = fopen(permission_file_path, "r");
    if(!permission_file) return ERROR_FILE_NOT_EXIST;
    clear(temp, sizeof(temp));
    
    char owner_test[BUFFER_LEN];
    char rw_test[BUFFER_LEN];

    clear(owner_test, sizeof(owner_test));
    clear(rw_test, sizeof(rw_test));

    sprintf(owner_test, "%s\t%d\n", user, PERMISSION_OWNER);
    sprintf(rw_test, "%s\t%d\n", user, PERMISSION_RW);
    int flag = 0;
    while(fgets(temp, sizeof(temp), permission_file)){
        if(!strcmp(temp, owner_test) || !strcmp(temp, rw_test)){
            // printf("--%s--%s--%d\n--%s--%s--%d\n", temp, owner_test, strcmp(temp, owner_test), temp, rw_test, strcmp(temp, rw_test));
            flag = 1;
            break;
        }
    }
    if(!flag) return ERROR_UNAUTHORIZED_OP;

    if(token && !strcmp(token, "INSERT")){
        token = strtok(NULL, " ");
        if(token && !strcmp(token, "INTO")){
            token = strtok(NULL, " ");
            if(token){
                char table_path[DIR_LEN * 2];
                clear(table_path, sizeof(table_path));
                sprintf(table_path, "%s/%s.txt", curr_db_path, token);
                printf("WE WANT TO INSERT INTO %s\n", table_path);

                //CHECK IF TABLE EXISTS
                FILE *table = fopen(table_path, "r");
                if(!table) return ERROR_FILE_NOT_EXIST;

                clear(temp, sizeof(temp));
                fgets(temp, sizeof(temp), table);
                clear(temp, sizeof(temp));
                fgets(temp, sizeof(temp), table);

                fclose(table);
                printf("DATATYPES %s\n", temp);
                
                token = strtok(NULL, " (");
                int val_count = 0;
                while(token){
                    printf("TOKEN %d: %s\n", val_count, token);
                    if(strcmp(token, "\n")) val_count++;
                    token = strtok(NULL, " ");
                }
                printf("VAL ARRAY OF SIZE %d\n", val_count);
                
                token = strtok(temp, "\t");
                int col_count = 0;
                while(token){
                    printf("TOKEN %d: %s\n", col_count, token);
                    if(strcmp(token, "\n")) col_count++;
                    token = strtok(NULL, "\t");
                }
                printf("COL ARRAY OF SIZE %d\n", col_count);

                if(col_count == val_count){
                    char val[val_count][BUFFER_LEN];
                    char col[col_count][BUFFER_LEN];

                    strcpy(command, msg);
                    char *token = strtok(command, " ");
                    token = strtok(NULL, " ");
                    token = strtok(NULL, " ");
                    token = strtok(NULL, " (");
                    val_count = 0;
                    while(token){
                        printf("TOKEN %d: %s\n", val_count, token);
                        if(strcmp(token, "\n")){
                            clear(val[val_count], strlen(val[val_count]));
                            strcpy(val[val_count], token);
                            printf("VAL[%d] = %s\n", val_count, val[val_count]);
                            while(val[val_count][strlen(val[val_count]) - 1] == '\n' 
                            || val[val_count][strlen(val[val_count]) - 1] == '\t'
                            || val[val_count][strlen(val[val_count]) - 1] == ' '
                            || val[val_count][strlen(val[val_count]) - 1] == ',' 
                            || val[val_count][strlen(val[val_count]) - 1] == ')'){
                                val[val_count][strlen(val[val_count]) - 1] = '\0';
                            }
                            val_count++;
                            printf("VAL[%d] = %s\n", val_count, val[val_count]);
                        }token = strtok(NULL, " ");
                    }

                    FILE *table = fopen(table_path, "r");
                    clear(temp, sizeof(temp));
                    fgets(temp, sizeof(temp), table);
                    clear(temp, sizeof(temp));
                    fgets(temp, sizeof(temp), table);
                    fclose(table);
                    
                    token = strtok(temp, "\t");
                    col_count = 0;
                    while(token){
                        printf("TOKEN %d: %s\n", col_count, token);
                        if(strcmp(token, "\n")){
                            clear(col[col_count], sizeof(col[col_count]));
                            strcpy(col[col_count], token);
                            printf("COL[%d] = %s\n", col_count, col[col_count]);
                            while(col[col_count][strlen(col[col_count]) - 1] == '\n' || col[col_count][strlen(col[col_count]) - 1] == '\t' || col[col_count][strlen(col[col_count]) - 1] == ')'){
                                col[col_count][strlen(col[col_count]) - 1] = '\0';
                            }
                            col_count++;
                            printf("COL[%d] = %s\n", col_count, col[col_count]);
                        }token = strtok(NULL, "\t");
                    }

                    // for(int i = 0; i < val_count; i++) printf("--%s--\n", val[i]);
                    // for(int i = 0; i < col_count; i++) printf("--%s--\n", col[i]);

                    char insert_line[BUFFER_LEN];
                    clear(insert_line, sizeof(insert_line));
                    strcpy(insert_line, "");

                    for(int i = 0; i < col_count; i++){
                        if(!strcmp(col[i], "int")){
                            if(!check_int(val[i])) return ERROR_INCOMPATIBLE_DATATYPE;
                            else{
                                strcat(insert_line, val[i]);
                                strcat(insert_line, "\t");
                            }
                        }else if(!strcmp(col[i], "float")){
                            if(!check_float(val[i])) return ERROR_INCOMPATIBLE_DATATYPE;
                            else{
                                strcat(insert_line, val[i]);
                                strcat(insert_line, "\t");
                            }
                        }else if(!strcmp(col[i], "string")){
                            if(!check_string(val[i])) return ERROR_INCOMPATIBLE_DATATYPE;
                            else{
                                strcat(insert_line, val[i]);
                                strcat(insert_line, "\t");
                            }
                        }
                    }

                    table = fopen(table_path, "a");
                    fprintf(table, "%s\n", insert_line);
                    fclose(table);
                    return SUCCESS_INSERT;
                }else return ERROR_SYNTAX;
            }else return ERROR_SYNTAX;
        }else return ERROR_SYNTAX;
    }else return ERROR_SYNTAX;
}

int sql_update(){
    if(!strcmp(curr_db_path, CUSTOM_DB_PATH)) return ERROR_NOT_CONNECTED;
    
    char permission_file_path[DIR_LEN * 2];
    sprintf(permission_file_path, "%s/%s", curr_db_path, PERMISSION_FILENAME);
    printf("PERMISSIONS FILE: %s\n", permission_file_path);
    FILE *permission_file = fopen(permission_file_path, "r");
    if(!permission_file) return ERROR_FILE_NOT_EXIST;
    clear(temp, sizeof(temp));
    
    char owner_test[BUFFER_LEN];
    char rw_test[BUFFER_LEN];

    clear(owner_test, sizeof(owner_test));
    clear(rw_test, sizeof(rw_test));

    sprintf(owner_test, "%s\t%d\n", user, PERMISSION_OWNER);
    sprintf(rw_test, "%s\t%d\n", user, PERMISSION_RW);
    int flag = 0;
    while(fgets(temp, sizeof(temp), permission_file)){
        if(!strcmp(temp, owner_test) || !strcmp(temp, rw_test)){
            flag = 1;
            break;
        }
    }
    if(!flag) return ERROR_UNAUTHORIZED_OP;
    fclose(permission_file);

    if(!strcmp(parsed_command[0], "UPDATE")){
        if(strlen(parsed_command[1])){
            //CHECK IF TABLE EXISTS
            char table_path[DIR_LEN * 2];
            clear(table_path, sizeof(table_path));
            sprintf(table_path, "%s/%s.txt", curr_db_path, parsed_command[1]);

            FILE *table = fopen(table_path, "r");
            if(!table) return ERROR_FILE_NOT_EXIST;
            //
            if(!strcmp(parsed_command[2], "SET")){
                if(strlen(parsed_command[3])){
                    //CHECK IF COLUMN EXISTS, SAVE INDEX IF EXIST
                    clear(temp, sizeof(temp));
                    fgets(temp, sizeof(temp), table);
                    int table_column_amt;
                    char** table_columns = create_token_array(temp, "\t", &table_column_amt);

                    printf("CHECKING COLUMNS....\n");
                    for(int i = 0; i < table_column_amt; i++) printf("---%s---\n", table_columns[i]);
                    printf("END CHECKING COLUMNS....\n");

                    int flag = 0;
                    int index = -1;
                    for(int i = 0; i < table_column_amt; i++){
                        if(!strcmp(table_columns[i], parsed_command[3])){
                            flag = 1;
                            index = i;
                            break;
                        }
                    }

                    if(!flag) return ERROR_COLUMN_NOT_EXIST;

                    printf("EXISTS\n");

                    //
                    if(!strcmp(parsed_command[4], "=")){
                        if(strlen(parsed_command[5])){
                            //CHECK DATATYPE
                            if(!check_int(parsed_command[5])
                            && !check_float(parsed_command[5])
                            && !check_string(parsed_command[5])
                            ) return ERROR_DATATYPE_NOT_EXIST;
                            //

                            clear(temp, sizeof(temp));
                            fgets(temp, sizeof(temp), table);
                            fclose(table);

                            char** table_datatypes = create_token_array(temp, "\t", &table_column_amt);

                            printf("CHECKING DATATYPES....\n");
                            for(int i = 0; i < table_column_amt; i++) printf("---%s---\n", table_datatypes[i]);
                            printf("END CHECKING DATATYPES....\n");

                            if((!strcmp(table_datatypes[index], "int") && !check_int(parsed_command[5]))
                            || (!strcmp(table_datatypes[index], "float") && !check_float(parsed_command[5]))
                            || (!strcmp(table_datatypes[index], "string") && !check_string(parsed_command[5]))
                            ) return ERROR_INCOMPATIBLE_DATATYPE;
                            
                            //CHECK WHERE CLAUSE
                            if(command_word_count == 10){
                                if(!strcmp(parsed_command[6], "WHERE")){
                                    if(strlen(parsed_command[7])){
                                        //CHECK IF COLUMN EXISTS, 
                                        //SAVE INDEX IF EXIST, 
                                        //SAVE DATA TYPE
                                        flag = 0;
                                        int where_col_index = -1;
                                        for(int i = 0; i < table_column_amt; i++){
                                            if(!strcmp(table_columns[i], parsed_command[7])){
                                                flag = 1;
                                                where_col_index = i;
                                                break;
                                            }
                                        }

                                        if(!flag) return ERROR_COLUMN_NOT_EXIST;

                                        //
                                        if(!strcmp(parsed_command[8], "=")){
                                            if(strlen(parsed_command[9])){
                                                //SAVE VALUE
                                                printf("CHECKING DATATYPES....\n");
                                                for(int i = 0; i < table_column_amt; i++) printf("---%s %s %d %d %d---\n", table_datatypes[i], parsed_command[7], check_int(parsed_command[7]), check_float(parsed_command[7]), check_string(parsed_command[7]));
                                                printf("END CHECKING DATATYPES....\n");

                                                if((!strcmp(table_datatypes[where_col_index], "int") && !check_int(parsed_command[9]))
                                                || (!strcmp(table_datatypes[where_col_index], "float") && !check_float(parsed_command[9]))
                                                || (!strcmp(table_datatypes[where_col_index], "string") && !check_string(parsed_command[9]))
                                                ) return ERROR_INCOMPATIBLE_DATATYPE;

                                                //
                                                //CHECK IF COLUMN EXISTS, SAVE INDEX IF EXIST

                                                char new_table_path[DIR_LEN * 2];
                                                sprintf(new_table_path, "%s/tempTable.txt", curr_db_path);
                                                FILE *new_table = fopen(new_table_path, "w");
                                                if(!new_table) return ERROR_OPEN;
                                                
                                                table = fopen(table_path, "r");
                                                
                                                clear(temp, sizeof(temp));
                                                fgets(temp, sizeof(temp), table);
                                                fprintf(new_table, "%s", temp);
                                                clear(temp, sizeof(temp));
                                                fgets(temp, sizeof(temp), table);
                                                fprintf(new_table, "%s", temp);

                                                int parsed_line_amt;
                                                char** parsed_line;

                                                while(fgets(temp, sizeof(temp), table)){
                                                    parsed_line_amt = 0;
                                                    char** parsed_line = create_token_array(temp, "\t", &parsed_line_amt);

                                                    if(!strcmp(parsed_line[where_col_index], parsed_command[9])){
                                                        clear(parsed_line[index], sizeof(parsed_line[index]));
                                                        strcpy(parsed_line[index], parsed_command[5]);
                                                    }

                                                    clear(temp, sizeof(temp));
                                                    for(int i = 0; i < parsed_line_amt ; i++){
                                                        if(strcmp(parsed_line[i], "\n")){
                                                            strcat(temp, parsed_line[i]);
                                                            strcat(temp, "\t");
                                                        }
                                                    }
                                                    strcat(temp, "\n");
                                                    
                                                    fprintf(new_table, "%s", temp);
                                                }

                                                fclose(table);
                                                fclose(new_table);

                                                remove(table_path);
                                                rename(new_table_path, table_path);

                                                printf("COMPLETED UPDATE WITH WHERE CLAUSE\n");
                                                return SUCCESS_UPDATE;
                                            }else return ERROR_SYNTAX;
                                        }else return ERROR_SYNTAX;
                                    }else return ERROR_SYNTAX;
                                }else return ERROR_SYNTAX;
                            }else if(command_word_count == 6){


                                char new_table_path[DIR_LEN * 2];
                                sprintf(new_table_path, "%s/tempTable.txt", curr_db_path);
                                FILE *new_table = fopen(new_table_path, "w");
                                if(!new_table) return ERROR_OPEN;
                                
                                table = fopen(table_path, "r");
                                
                                clear(temp, sizeof(temp));
                                fgets(temp, sizeof(temp), table);
                                fprintf(new_table, "%s", temp);
                                clear(temp, sizeof(temp));
                                fgets(temp, sizeof(temp), table);
                                fprintf(new_table, "%s", temp);

                                int parsed_line_amt;
                                char** parsed_line;

                                while(fgets(temp, sizeof(temp), table)){
                                    parsed_line_amt = 0;
                                    char** parsed_line = create_token_array(temp, "\t", &parsed_line_amt);

                                    clear(parsed_line[index], sizeof(parsed_line[index]));
                                    strcpy(parsed_line[index], parsed_command[5]);

                                    clear(temp, sizeof(temp));
                                    for(int i = 0; i < parsed_line_amt; i++){
                                        if(strcmp(parsed_line[i], "\n")){
                                            strcat(temp, parsed_line[i]);
                                            strcat(temp, "\t");
                                        }
                                    }
                                    strcat(temp, "\n");
                                    
                                    fprintf(new_table, "%s", temp);
                                }

                                fclose(table);
                                fclose(new_table);

                                remove(table_path);
                                rename(new_table_path, table_path);

                                printf("COMPLETED UPDATE WITHOUT CLAUSE\n");
                                return SUCCESS_UPDATE;
                            }else return ERROR_SYNTAX;
                        }else return ERROR_SYNTAX;
                    }else return ERROR_SYNTAX;
                }else return ERROR_SYNTAX;
            }else return ERROR_SYNTAX;    
        }else return ERROR_SYNTAX;
    }else return ERROR_SYNTAX;
    return 0;
}

int sql_delete(){
    if(!strcmp(curr_db_path, CUSTOM_DB_PATH)) return ERROR_NOT_CONNECTED;
    
    char permission_file_path[DIR_LEN * 2];
    sprintf(permission_file_path, "%s/%s", curr_db_path, PERMISSION_FILENAME);
    printf("PERMISSIONS FILE: %s-----\n", permission_file_path);
    FILE *permission_file = fopen(permission_file_path, "r");
    // if(!permission_file) printf("FILE POINTER %d\n", permission_file);
    clear(temp, sizeof(temp));
    
    char owner_test[BUFFER_LEN];
    char rw_test[BUFFER_LEN];

    clear(owner_test, sizeof(owner_test));
    clear(rw_test, sizeof(rw_test));

    sprintf(owner_test, "%s\t%d\n", user, PERMISSION_OWNER);
    sprintf(rw_test, "%s\t%d\n", user, PERMISSION_RW);
    int flag = 0;
    while(fgets(temp, sizeof(temp), permission_file)){
        if(!strcmp(temp, owner_test) || !strcmp(temp, rw_test)){
            flag = 1;
            break;
        }
    }
    if(!flag) return ERROR_UNAUTHORIZED_OP;
    fclose(permission_file);

    if(!strcmp(parsed_command[0], "DELETE")){
        if(!strcmp(parsed_command[1], "FROM")){
            if(strlen(parsed_command[2])){
                //CHECK IF TABLE EXISTS
                char table_path[DIR_LEN * 2];
                clear(table_path, sizeof(table_path));
                sprintf(table_path, "%s/%s.txt", curr_db_path, parsed_command[2]);

                FILE *table = fopen(table_path, "r");
                if(!table) return ERROR_FILE_NOT_EXIST;
                
                if(command_word_count == 7){
                    if(!strcmp(parsed_command[3], "WHERE")){
                        if(strlen(parsed_command[4])){
                            //CHECK IF COLUMN EXISTS, 
                            //SAVE INDEX IF EXIST, 
                            //SAVE DATA TYPE
                            clear(temp, sizeof(temp));
                            fgets(temp, sizeof(temp), table);

                            int table_column_amt;
                            char** table_columns = create_token_array(temp, "\t", &table_column_amt);

                            int flag = 0;
                            int where_col_index = -1;
                            for(int i = 0; i < table_column_amt; i++){
                                if(!strcmp(table_columns[i], parsed_command[4])){
                                    flag = 1;
                                    where_col_index = i;
                                    break;
                                }
                            }

                            if(!flag) return ERROR_COLUMN_NOT_EXIST;

                            clear(temp, sizeof(temp));
                            fgets(temp, sizeof(temp), table);

                            table_column_amt;
                            char** table_datatypes = create_token_array(temp, "\t", &table_column_amt);

                            //
                            if(!strcmp(parsed_command[5], "=")){
                                if(strlen(parsed_command[6])){

                                    //SAVE VALUE
                                    printf("CHECKING DATATYPES....\n");
                                    for(int i = 0; i < table_column_amt; i++) printf("---%s %s %d %d %d---\n", table_datatypes[i], parsed_command[6], check_int(parsed_command[6]), check_float(parsed_command[6]), check_string(parsed_command[6]));
                                    printf("END CHECKING DATATYPES....\n");

                                    if((!strcmp(table_datatypes[where_col_index], "int") && !check_int(parsed_command[6]))
                                    || (!strcmp(table_datatypes[where_col_index], "float") && !check_float(parsed_command[6]))
                                    || (!strcmp(table_datatypes[where_col_index], "string") && !check_string(parsed_command[6]))
                                    ) return ERROR_INCOMPATIBLE_DATATYPE;

                                    //
                                    //CHECK IF COLUMN EXISTS, SAVE INDEX IF EXIST

                                    char new_table_path[DIR_LEN * 2];
                                    sprintf(new_table_path, "%s/tempTable.txt", curr_db_path);
                                    FILE *new_table = fopen(new_table_path, "w");
                                    if(!new_table) return ERROR_OPEN;
                                    
                                    fclose(table);
                                    table = fopen(table_path, "r");
                                    
                                    clear(temp, sizeof(temp));
                                    fgets(temp, sizeof(temp), table);
                                    fprintf(new_table, "%s", temp);
                                    clear(temp, sizeof(temp));
                                    fgets(temp, sizeof(temp), table);
                                    fprintf(new_table, "%s", temp);

                                    int parsed_line_amt;
                                    char** parsed_line;

                                    while(fgets(temp, sizeof(temp), table)){
                                        parsed_line_amt = 0;
                                        char** parsed_line = create_token_array(temp, "\t", &parsed_line_amt);

                                        if(strcmp(parsed_line[where_col_index], parsed_command[6])){
                                            fprintf(new_table, "%s", temp);
                                        }
                                    }

                                    fclose(table);
                                    fclose(new_table);

                                    remove(table_path);
                                    rename(new_table_path, table_path);

                                    printf("COMPLETED DELETE WITH WHERE CLAUSE\n");
                                    return SUCCESS_DELETE;
                                }else return ERROR_SYNTAX;
                            }else return ERROR_SYNTAX;
                        }else return ERROR_SYNTAX;
                    }else return ERROR_SYNTAX;
                }else if(command_word_count == 3){
                    char new_table_path[DIR_LEN * 2];
                    sprintf(new_table_path, "%s/tempTable.txt", curr_db_path);
                    FILE *new_table = fopen(new_table_path, "w");
                    if(!new_table) return ERROR_OPEN;
                    
                    table = fopen(table_path, "r");
                    
                    clear(temp, sizeof(temp));
                    fgets(temp, sizeof(temp), table);
                    fprintf(new_table, "%s", temp);
                    clear(temp, sizeof(temp));
                    fgets(temp, sizeof(temp), table);
                    fprintf(new_table, "%s", temp);

                    fclose(table);
                    fclose(new_table);

                    remove(table_path);
                    rename(new_table_path, table_path);

                    printf("COMPLETED UPDATE WITHOUT CLAUSE\n");
                    return SUCCESS_DELETE;
                }else return ERROR_SYNTAX;
            }else return ERROR_SYNTAX;
        }else return ERROR_SYNTAX;
    }else return ERROR_SYNTAX;    
    return 0;
}

int sql_select(){
    // if(!strcmp(curr_db_path, CUSTOM_DB_PATH)) return ERROR_NOT_CONNECTED;
    
    if(command_word_count < 4) return ERROR_SYNTAX;
    //find where clause
    int where_clause_index = 0;
    for(int i = 0; i < command_word_count; i++){
        if(!strcmp(parsed_command[i], "WHERE")){
            where_clause_index = i;
            break;
        }
    }
    if(where_clause_index && where_clause_index < 4) return ERROR_SYNTAX;
    int select_clause_end = (where_clause_index) ? where_clause_index - 1 : command_word_count - 1;
    
    if(where_clause_index) printf("SELECT HAS WHERE CLAUSE\n");
    else printf("SELECT HAS NO WHERE CLAUSE\n");

    //process select clause
    if(!strcmp(parsed_command[0], "SELECT") 
    && !strcmp(parsed_command[select_clause_end - 1], "FROM")){
        
        //check table exists
        char table_path[DIR_LEN * 2];
        sprintf(table_path, "%s/%s.txt", curr_db_path, parsed_command[select_clause_end]);
        FILE *table = fopen(table_path, "r");
        if(!table) return ERROR_FILE_NOT_EXIST;
        
        printf("\n\n\n\nSELECT COMMAND INITIATED...\n\n\n\n");
        int input_col_count = select_clause_end - 2;
        char** input_col_name = (char**) malloc(input_col_count * sizeof(char*));
        for(int i = 0; i < input_col_count; i++){
            input_col_name[i] = (char*) malloc(BUFFER_LEN * sizeof(char));
        }

        int is_select_all = 0;
        for(int i = 1; i <= input_col_count; i++){
            clear(input_col_name[i-1], sizeof(input_col_name[i-1]));
            strcpy(input_col_name[i-1], parsed_command[i]);
            while(input_col_name[i-1][strlen(input_col_name[i-1]) - 1] == '\n'
            || input_col_name[i-1][strlen(input_col_name[i-1]) - 1] == '\t'
            || input_col_name[i-1][strlen(input_col_name[i-1]) - 1] == ' '
            || input_col_name[i-1][strlen(input_col_name[i-1]) - 1] == ',') input_col_name[i-1][strlen(input_col_name[i-1] ) - 1] = '\0'; 
            printf("COL %d: ---%s---\n", i, input_col_name[i-1]);

            if(!strcmp(input_col_name[i-1], "*")){
                if(input_col_count == 1) is_select_all = 1;
                else return ERROR_SYNTAX;
            }
        }

        //check that all column names exist in table
        int* input_col_index;
        clear(temp, sizeof(temp));
        fgets(temp, sizeof(temp), table);

        int table_col_count;
        char** table_col_name = create_token_array(temp, "\t", &table_col_count);
        table_col_count--;
        if(is_select_all){
            input_col_count = table_col_count;
            input_col_name = table_col_name;
            input_col_index = (int*) malloc(input_col_count * sizeof(int));
            for(int i = 0; i < input_col_count; i++){
                input_col_index[i] = i;
            }
        }else{
            input_col_index = (int*) malloc(input_col_count * sizeof(int));
            for(int i = 0; i < input_col_count; i++){
                int flag = 0;
                for(int j = 0; j < table_col_count; j++){
                    printf("-%s- -%s-\n", input_col_name[i], table_col_name[j]);
                    if(!strcmp(input_col_name[i], table_col_name[j])){
                        input_col_index[i] = j;
                        flag = 1;
                        break;
                    }
                }
                if(!flag) return ERROR_COLUMN_NOT_EXIST;
            }
        }
        

        clear(temp, sizeof(temp));
        fgets(temp, sizeof(temp), table);

        int where_col_index = -1;
        
        if(where_clause_index){
            if(command_word_count - where_clause_index == 4){
                if(!strcmp(parsed_command[where_clause_index], "WHERE")
                && !strcmp(parsed_command[command_word_count - 2], "=")){
                    //column exist
                    int flag = 0;
                    for(int i = 0; i < table_col_count; i++){
                        if(!strcmp(parsed_command[where_clause_index + 1], table_col_name[i])){
                            where_col_index = i;
                            flag = 1;
                            break;
                        }
                    }
                    if(!flag) return ERROR_COLUMN_NOT_EXIST;

                    //value is same datatype as column

                    char **table_col_datatype = create_token_array(temp, "\t", &table_col_count);
                    table_col_count--;

                    if((!strcmp(table_col_datatype[where_col_index], "int") && !check_int(parsed_command[command_word_count - 1]))
                    || (!strcmp(table_col_datatype[where_col_index], "float") && !check_float(parsed_command[command_word_count - 1]))
                    || (!strcmp(table_col_datatype[where_col_index], "string") && !check_string(parsed_command[command_word_count - 1]))
                    ) return ERROR_INCOMPATIBLE_DATATYPE;

                    

                }else return ERROR_SYNTAX;
            }else return ERROR_SYNTAX;
        }
           
        clear(temp, sizeof(temp));

        int table_data_count = 0;
        while(fgets(temp, sizeof(temp), table)){
            clear(temp, sizeof(temp));
            table_data_count++;
        }

        printf("TABLE HAS %d RECORDS\n", table_data_count);
        fclose(table);
        
        // send amount of lines to be sent
        //
        //
        //
        //
        //
        //
        // send amount of lines to be sent

        table = fopen(table_path, "r");
        clear(temp, sizeof(temp));
        fgets(temp, sizeof(temp), table);
        clear(temp, sizeof(temp));
        fgets(temp, sizeof(temp), table);
        clear(temp, sizeof(temp));
        
        for(int i = 0; i < input_col_count; i++){
            strcat(temp, table_col_name[input_col_index[i]]);
            strcat(temp, "\t");    
        }

        write_msg(temp);
        printf("---%s---\n", temp);

        char output_line[BUFFER_LEN];
        char** parsed_line;
        int row_count = 0;
        char output_lines[table_data_count][BUFFER_LEN];

        for(int i = 0; i < table_data_count; i++){
            clear(temp, sizeof(temp));
            fgets(temp, sizeof(temp), table);
            clear(output_line, sizeof(output_line));

            parsed_line = create_token_array(temp, "\t", &table_col_count);
            table_col_count--;

            if(where_clause_index){
                if(strcmp(parsed_line[where_col_index], parsed_command[command_word_count - 1])) continue;
            }

            for(int j = 0; j < input_col_count; j++){
                    strcat(output_line, parsed_line[input_col_index[j]]);
                    strcat(output_line, "\t");
            }

            printf("---%s---\n", output_line);
            sprintf(output_lines[row_count], "%s", output_line);
            row_count++;
        }

        char table_data_count_information[BUFFER_LEN];
        sprintf(table_data_count_information, "%d", row_count);
        write_msg(table_data_count_information);

        for(int i = 0; i < row_count; i++){
            write_msg(output_lines[i]);
        }
        return SUCCESS_SELECT;
    }else return ERROR_SYNTAX;

    return 0;
}

void write_log(){

    time_t timer;
    struct tm* curr_time;

    time(&timer);
    curr_time = localtime(&timer);
    char timestamp[BUFFER_LEN];
    clear(timestamp, sizeof(timestamp));
    strftime(timestamp, sizeof(timestamp), "%F %T", curr_time);

    clear(temp, sizeof(temp));
    for(int i = 0; i < command_word_count; i++){
        strcat(temp, parsed_command[i]);
        if(i < command_word_count - 1) strcat(temp, " ");
    }

    FILE* log_file = fopen(LOG_PATH, "a");
    if(!log_file) return ERROR_OPEN;
    fprintf(log_file, "%s:%s:%s:%s\n", timestamp, user, temp, output_msg);
    printf("%s:%s:%s:%s\n", timestamp, user, temp, output_msg);
    fclose(log_file);
}

int main(){
    // //CREATE DAEMON
    // pid_t pid;        
    // pid = fork();     
    // if (pid < 0) exit(EXIT_FAILURE);
    // if (pid > 0) exit(EXIT_SUCCESS);

    // //SET DAEMON UP
    // umask(0);
    // pid_t sid = setsid();
    // if (sid < 0) exit(EXIT_FAILURE);
    // if ((chdir("/")) < 0) exit(EXIT_FAILURE);
    // close(STDIN_FILENO);
    // close(STDOUT_FILENO);
    // close(STDERR_FILENO);

    //CREATE SOCKET
    server_socket = socket(AF_INET, SOCK_STREAM, 0);
    check(server_socket, ERROR_SOCKET_CREATE);

    //SET SOCKET UP
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(LOCALPORT);
    server_address.sin_addr.s_addr = INADDR_ANY;

    //BIND
    bind_status = bind(server_socket, (struct sockaddr *) &server_address, sizeof(server_address));
    check(bind_status, ERROR_SOCKET_BIND);

    //LISTEN
    listen_status = listen(server_socket, 5);
    check(listen_status, ERROR_SOCKET_LISTEN);

    mkdir(CUSTOM_DB_PATH, 0777);
    //DAEMON LOOP

    while (1) {
        // DAEMON PROGRAM
        client_socket = accept(server_socket, (struct sockaddr *) &server_address, (socklen_t *) &addr_size);
        if(client_socket >= 0){
            strcpy(curr_db_path, CUSTOM_DB_PATH);
            clear(output_msg, sizeof(output_msg));
            clear(input_msg, sizeof(input_msg));
            
            //GET USER TYPE
            read_msg();
            if(!strcmp(input_msg, "ROOT")) {
                uid = ROOT_UID;
                write_msg("LOGIN AS ROOT SUCCESS");
            }else {
                uid = USER_UID;
                int flag = 0;
                clear(temp, sizeof(temp));
                strcpy(temp, input_msg);
                if(!login_check(temp)) flag = 1;
                if(flag){
                    write_msg("LOGIN FAILED");
                    close(client_socket);
                    continue;
                }
                write_msg("LOGIN SUCCESS");
            }
        
            while(1){
                printf("\n");
                send_curr_path();
                //RECEIVE QUERY
                //QREAD 1
                read_msg();
                
                command_word_count;
                parsed_command = create_token_array(input_msg, " ", &command_word_count);
                for(int i = 0; i < command_word_count; i++) printf("%s\n", parsed_command[i]);

                //SEND CONFIRMATION
                //QWRITE 1
                clear(temp, sizeof(temp));
                strcpy(temp, input_msg);
                write_msg(temp);
                
                //END PROGRAM IF REQUESTED
                if(!strcmp(input_msg, EXIT_COMMAND)) break;

                int status = 0;
                if(!strcmp(parsed_command[0], "CREATE")){
                    status = sql_create(input_msg, uid);
                }else if(!strcmp(parsed_command[0], "USE")){
                    status = sql_use(input_msg, uid);
                }else if(!strcmp(parsed_command[0], "DISCONNECT")){
                    status = sql_disconnect(input_msg, uid);
                }else if(!strcmp(parsed_command[0], "GRANT")){
                    status = sql_grant(input_msg, uid);
                }else if(!strcmp(parsed_command[0], "DROP")){
                    status = sql_drop(input_msg, uid);
                }else if(!strcmp(parsed_command[0], "INSERT")){
                    status = sql_insert(input_msg, uid);
                }else if(!strcmp(parsed_command[0], "UPDATE")){
                    status = sql_update();
                }else if(!strcmp(parsed_command[0], "DELETE")){
                    status = sql_delete();
                }else if(!strcmp(parsed_command[0], "SELECT")){
                    status = sql_select();
                }else status = ERROR_UNRECOGNIZED_CMD;
                
                //SEND QUERY RESULT
                //QWRITE 2
                msg(status);
                write_log();
            }
        }
    }

    return 0;
}
